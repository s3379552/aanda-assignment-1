a1: a1.c a1.h a1_io.o a1_linked.o a1_array.o a1_hash.o
	gcc -g -ansi -Wall -pedantic a1.c a1_io.o a1_linked.o a1_array.o a1_hash.o -o a1
io: a1_io.c a1_io.h
	gcc -g -ansi -Wall -pedantic -c a1_io.c
linked: a1_linked.c a1_linked.h
	gcc -g -ansi -Wall -pedantic -c a1_linked.c
array: a1_array.c a1_array.h
	gcc -g -ansi -Wall -pedantic -c a1_array.c
hash: a1_hash.c a1_hash.h
	gcc -g -ansi -Wall -pedantic -c a1_hash.c 
clean:
	rm -f a1 *.o
