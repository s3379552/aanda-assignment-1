#include "a1_io.h"

struct name_list * read_names_list(char * filename, name_list * n_list)
{
	FILE * fp;
	char * hname = malloc(sizeof(char) * 50);
	char * tempstring;
	char * n_name;
	char * n_origin;
	char * n_meaning;
	char * line_token;
	unsigned long total_time = 0;
	unsigned long start_time, end_time;
	struct name_node * new_node;
	char attr_delim[2] = "\t";
	
	/*opening file */
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "File open failure.\n");
		exit(EXIT_FAILURE);
	}
	tempstring = (char *) malloc(MAX_LINE);
	assert(tempstring != NULL);
	
	/*Taking line of text from file until EOF is reached*/
	while (fgets(tempstring, MAX_LINE+1, fp) != NULL)
	{
		tempstring[strlen(tempstring) - 1] = '\0';

		/*Tokenising string for name, origin and meaning*/
		line_token = strtok(tempstring, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading NAME line token.\n");
			exit(EXIT_FAILURE);
		}
		n_name = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading ORIGIN line token.\n");
			exit(EXIT_FAILURE);
		}
		n_origin = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading MEANING line token.\n");
			exit(EXIT_FAILURE);
		}
		n_meaning = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token != NULL)
		{
			fprintf(stderr, "Error reading EOL line token.\n");
			exit(EXIT_FAILURE);
		}
		
		/*Initialising new element with tokenised values*/
		new_node = malloc(sizeof(struct name_node));
		new_node->name = malloc((strlen(n_name)+1)*sizeof(char));
		new_node->origin = malloc((strlen(n_origin)+1)*sizeof(char));
		new_node->meaning = malloc((strlen(n_meaning)+1)*sizeof(char));
		strcpy(new_node->name, n_name);
		strcpy(new_node->origin, n_origin);
		strcpy(new_node->meaning, n_meaning);
		new_node->next_node = NULL;

		/* Inserting new element */
  		start_time = gettime();
  		n_list = add_node(new_node, n_list);
 		end_time = gettime();
		total_time +=(end_time-start_time);
	}
	/* Displaying total insertion time and closing the file */	
	printf("TOTAL LIST INSERTION TIME TAKEN: %.2f\n", (double)total_time/1E6);
	fclose(fp);
	free(tempstring);
	return n_list;
}

struct name_data **  read_names_array (char * filename, name_data ** n_array, int * size)
{
	FILE * fp;
	char * tempstring;
	char * n_name;
	char * n_origin;
	char * n_meaning;
	char * line_token;
	unsigned long total_time = 0;
	unsigned long start_time, end_time;
	struct name_data * new_data;
	char attr_delim[2] = "\t";
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "File open failure.\n");
		exit(EXIT_FAILURE);
	}
	tempstring = (char *) malloc(MAX_LINE);
	assert(tempstring != NULL);
	while (fgets(tempstring, MAX_LINE+1, fp) != NULL)
	{
		new_data = malloc(sizeof(struct name_data));
		tempstring[strlen(tempstring) - 1] = '\0';
		line_token = strtok(tempstring, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading NAME line token.\n");
			exit(EXIT_FAILURE);
		}
		n_name = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading ORIGIN line token.\n");
			exit(EXIT_FAILURE);
		}
		n_origin = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading MEANING line token.\n");
			exit(EXIT_FAILURE);
		}
		n_meaning = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token != NULL)
		{
			fprintf(stderr, "Error reading EOL line token.\n");
			exit(EXIT_FAILURE);
		}
		
  		new_data->name = malloc((strlen(n_name)+1)*sizeof(char));
		new_data->origin = malloc((strlen(n_origin)+1)*sizeof(char));
		new_data->meaning = malloc((strlen(n_meaning)+1)*sizeof(char));
		strcpy(new_data->name, n_name);
		strcpy(new_data->origin, n_origin);
		strcpy(new_data->meaning, n_meaning);
  		start_time = gettime();
  		n_array = add_name(new_data, n_array, size); 
  		end_time = gettime();
		total_time += end_time - start_time;	
	}
	printf("TOTAL INSERTION TIME TAKEN: %.2f\n", (double)total_time/1E6);
	fclose(fp);
	free(tempstring);
	return n_array;
}

struct hash_table * read_names_hash(char * filename, hash_table * n_table)
{
	FILE * fp;
	char * tempstring;
	char * n_name;
	char * n_origin;
	char * n_meaning;
	char * line_token;
	int h_value;
	unsigned long h_total_time = 0, i_total_time = 0;
	unsigned long i_start_time, i_end_time, h_start_time, h_end_time;
	hash_entry * new_entry;
	char attr_delim[2] = "\t";
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "File open failure.\n");
		exit(EXIT_FAILURE);
	}
	tempstring = (char *) malloc(MAX_LINE);
	assert(tempstring != NULL);
	while (fgets(tempstring, MAX_LINE+1, fp) != NULL)
	{
		tempstring[strlen(tempstring) - 1] = '\0';
		line_token = strtok(tempstring, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading NAME line token.\n");
			exit(EXIT_FAILURE);
		}
		n_name = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading ORIGIN line token.\n");
			exit(EXIT_FAILURE);
		}
		n_origin = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading MEANING line token.\n");
			exit(EXIT_FAILURE);
		}
		n_meaning = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token != NULL)
		{
			fprintf(stderr, "Error reading EOL line token.\n");
			exit(EXIT_FAILURE);
		}
		
		new_entry = malloc(sizeof(struct hash_entry));
		new_entry->name = malloc((strlen(n_name)+1)*sizeof(char));
		new_entry->origin = malloc((strlen(n_origin)+1)*sizeof(char));
		new_entry->meaning = malloc((strlen(n_meaning)+1)*sizeof(char));
		strcpy(new_entry->name, n_name);
		strcpy(new_entry->origin, n_origin);
		strcpy(new_entry->meaning, n_meaning);
		new_entry->next_entry = NULL;

		/* The hash table version of the insertion function times
 * 		the time taken to hash the name as well as insertion time */
		h_start_time = gettime();
		h_value = hash_gen(n_name);
		h_end_time = gettime();
		h_total_time += h_end_time - h_start_time;
  		i_start_time = gettime();
  		hash_insert(new_entry, n_table, h_value);
 		i_end_time = gettime();
		i_total_time += i_end_time - i_start_time;
	}
	printf("TOTAL HASH TABLE HASHING TIME TAKEN: %.4f\n", (double) h_total_time/1E6);
	printf("TOTAL HASH TABLE INSERTION TIME TAKEN: %.4f\n", (double) i_total_time/1E6);
	fclose(fp);
	free(tempstring);
	return n_table;
}

void  search_list (char * filename, name_list * n_list)
{
	FILE * fp;
	char * s_name, * tempstring;
	unsigned long d_total_time = 0, s_total_time = 0;
	unsigned long d_start_time, d_end_time, s_start_time, s_end_time;
	name_node * return_node;
	int print_choice;
	printf("Press 1 to print search results, print 0 not print results: ");
	scanf("%d", &print_choice);
	/* Opening the file with list of names to search */
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "File open failure.\n");
		exit(EXIT_FAILURE);
	}
	tempstring = (char *) malloc(MAX_LINE+1);
	assert(tempstring != NULL);
	
	/* Get 1 line (1 name per line) from the file until EOF*/
	while (fgets(tempstring, MAX_LINE+1, fp) != NULL)
	{
		tempstring[strlen(tempstring) - 1] = '\0';
		s_name = malloc((strlen(tempstring)+1)*sizeof(char));
	
		/* Tokenising the line to get rid of extra characters */
		s_name = strtok(tempstring, "\r");

		/*Timing both dumb and smart search times */
		d_start_time = gettime();
		return_node = dumb_search_node(n_list, s_name);
		d_end_time = gettime();
		d_total_time += d_end_time - d_start_time;
		if(print_choice == 1)
		{
			if(return_node != NULL)
			{
				while(strcmp(return_node->name, s_name) == 0)
				{
					printf("Name: %s, Origin: %s, Meaning: %s\n", return_node->name, return_node->origin, return_node->meaning);
					return_node = return_node->next_node;
				} 
			}
			else
			{
				printf("Name %s not found.\n", s_name);
			}
		}		
		s_start_time = gettime();
		smart_search_node(n_list, s_name);
		s_end_time = gettime();
		s_total_time += s_end_time - s_start_time;
	}

	/* Printing search time results and closing the file */
	printf("Total dumb search time: %.2f\n", (double) d_total_time/1E6);
	printf("Total smart search time: %.2f\n", (double) s_total_time/1E6);
	fclose(fp);
	free(s_name);
	return;
}

void  search_array (char * filename, name_data ** n_array, int * size)
{
	FILE * fp;
	char * s_name, * tempstring;
	unsigned long d_total_time = 0, s_total_time = 0;
	unsigned long d_start_time, d_end_time, s_start_time, s_end_time;
	int return_data, return_max, return_min;
	int print_choice;
	printf("Press 1 to print search results, print 0 not print results: ");
	scanf("%d", &print_choice);
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "File open failure.\n");
		exit(EXIT_FAILURE);
	}
	tempstring = (char *) malloc(MAX_LINE+1);
	assert(tempstring != NULL);
	while (fgets(tempstring, MAX_LINE+1, fp) != NULL)
	{
		tempstring[strlen(tempstring) - 1] = '\0';
		s_name = malloc((strlen(tempstring)+1)*sizeof(char));
		s_name = strtok(tempstring, "\r");
		d_start_time = gettime();
		return_data = dumb_search_array(n_array, s_name, size);
		d_end_time = gettime();
		d_total_time += d_end_time - d_start_time;		

		if(print_choice == 1)
		{
			if(return_data != -1)
			{	
				while(strcmp(n_array[return_data]->name, s_name) == 0)
				{
					printf("Name: %s, Origin: %s, Meaning: %s\n", n_array[return_data]->name,
						n_array[return_data]->origin, n_array[return_data]->meaning);
					return_data++;
				}
			}
			else
				printf("Name %s not found.\n", s_name);		
		}
		s_start_time = gettime();
		return_data = smart_search_array(n_array, s_name, size);
		s_end_time = gettime();
		s_total_time += s_end_time - s_start_time;
		return_min = return_data;
		return_max = return_data;
		if(print_choice == 1)
		{
			if(return_data != -1)
			{
				while(strcmp(n_array[return_min]->name, s_name) == 0)
					return_min--;
				while(strcmp(n_array[return_max]->name, s_name) == 0)
					return_max++;
				return_min++;
				return_max--;
				while(return_min <= return_max)
				{
					printf("Name: %s, Origin: %s, Meaning: %s\n", n_array[return_min]->name,
						n_array[return_min]->origin, n_array[return_min]->meaning);
					return_min++;
				}
			}
			else
				printf("Name %s not found.\n", s_name);
		}
	}
	printf("Total dumb search time: %.2f\n", (double) d_total_time/1E6);
	printf("Total smart search time: %.4f\n", (double) s_total_time/1E6);
	fclose(fp);
	free(s_name);
	return;
}

void  search_table (char * filename, hash_table * n_table)
{
	FILE * fp;
	char * s_name, * tempstring;
	unsigned long d_total_time = 0, s_total_time = 0;
	unsigned long d_start_time, d_end_time, s_start_time, s_end_time;
	hash_bucket * return_bucket;
	hash_entry * return_entry;
	int print_choice;
	printf("Press 1 to print search results, print 0 not print results: ");
	scanf("%d", &print_choice);

	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "File open failure.\n");
		exit(EXIT_FAILURE);
	}
	tempstring = (char *) malloc(MAX_LINE+1);
	assert(tempstring != NULL);
	while (fgets(tempstring, MAX_LINE+1, fp) != NULL)
	{
		tempstring[strlen(tempstring) - 1] = '\0';
		s_name = malloc((strlen(tempstring)+1)*sizeof(char));
		s_name = strtok(tempstring, "\r");
		d_start_time = gettime();
		return_bucket = hash_dumb_lookup(n_table, s_name);
		d_end_time = gettime();
		d_total_time += d_end_time - d_start_time;		
		
		if(print_choice == 1)
		{
			if(return_bucket != NULL)
			{
				return_entry = return_bucket->first_entry;
				while(return_entry != NULL)
				{
					printf("Name: %s, Origin: %s, Meaning: %s\n", return_entry->name,
						return_entry->origin, return_entry->meaning);
					return_entry = return_entry->next_entry;
				}
			}
			else
			{
				printf("Name %s not found.\n", s_name);
			}
		}
		
		s_start_time = gettime();
		return_bucket = hash_smart_lookup(n_table, s_name);
		s_end_time = gettime();
		s_total_time += s_end_time - s_start_time;

		if(print_choice == 1)
		{
			if(return_bucket != NULL)
			{
				return_entry = return_bucket->first_entry;
				while(return_entry != NULL)
				{
					printf("Name: %s, Origin: %s, Meaning: %s\n", return_entry->name,
						return_entry->origin, return_entry->meaning);
					return_entry = return_entry->next_entry;
				}
			}
			else
			{
				printf("Name %s not found.\n", s_name);
			}
		}
	}
	printf("Total dumb search time: %.4f\n", (double) d_total_time/1E6);
	printf("Total smart search time: %.4f\n", (double) s_total_time/1E6);
	fclose(fp);
	free(s_name);
	return;
}

