#include "a1_array.h"

struct name_data ** create_array()
{

	/* Initialising array with space for one element */
	struct name_data ** name_array;
	name_array = malloc(sizeof(struct name_data));
	*name_array = NULL;
	return name_array;
}

struct name_data ** add_name(name_data * new_name, name_data ** name_array, int * size)
{
	/* Reallocating space for one more element */
	name_array = realloc(name_array, sizeof(struct name_data)*(*size+1));
	
	/* Inserting element at end of array */
	name_array[*size] = new_name;
	
	/* Incrementing size counter */
	*size += 1;
	return name_array;
}

/* Code structure taken from
 * diogopinho.hubpages.com/hub/easy-quicksort-with-examples
 * by DiogoPinho*/
struct name_data ** qsort_array(name_data ** name_array, int left, int right)
{
	/* Initialise left and right trackers */
	int i = left, j = right - 1;
	struct name_data * tmp_data;

	/* Calculate new middle value */
	int middle = ((right-left)/2) + left;

	/* Get pivote name */
	char * pivot = name_array[middle]->name;	
	while(i <= j)
	{
		/* Find first name from left side greater than pivot */
		while(strcmp(name_array[i]->name, pivot) < 0)
			i++;

		/* Find first name from right less than pivot */
		while(strcmp(name_array[j]->name, pivot) > 0)
			j--;

		/* If left tracker is still left of pivot and right tracker
 * 		is still right of pivot, swap values */
		if(i <= j)
		{
			tmp_data = name_array[i];
			name_array[i] = name_array[j];
			name_array[j] = tmp_data;
			i++;
			j--;
		}
	};

	/* If left bound is still less than right tracker */
	if(left < j)
		qsort_array(name_array, left, j);

	/* If right bound is still greater than left tracker */
	if(i < right)
		qsort_array(name_array, i, right);
	return name_array;
}

/* Code structure and basis taken from
 * www.rosettacode.org/wiki/Sorting_algorithms/Insertion_sort */
struct name_data ** isort_array(name_data ** name_array, int * max)
{
	int i, j;
	struct name_data * tmp_data;
	for(i = 1; i < *max; i++)
	{
		
		/* Get new compare value */
		tmp_data = name_array[i];

		/* While elements before compare value are still greater than it*/
		for(j = i; j > 0 && strcmp(tmp_data->name, name_array[j-1]->name) < 0; j--)
		{
			name_array[j] = name_array[j-1];
		}
		
		/* Place compare value in ending position */
		name_array[j] = tmp_data;
	}
	return name_array;
}
int dumb_search_array(name_data ** name_array, char * s_name, int *max)
{
	int i;
	for(i = 0; i < *max; i++)
	{
		
		/* Compare current and requested values */
		if(strcmp(s_name, name_array[i]->name) == 0)
			return i;
			/*return name_array[i];*/
	}
	return -1;
} 


/* Code structure and basis taken from 
 * "C program for binary search"
 * www.programmingsimplified.com/c/source-code/c-program-binary-search */
int smart_search_array(name_data ** name_array, char * s_name, int *max)
{
	/* Binary Search */
	int first = 0, middle, last = (*max-1);

	/* Calculate middle value */
	middle = ((last-first)/2) + first;
	
	while( last > first)
	{
		
		/* Name is in bottom half of array bounds */
		if(strcmp(name_array[middle]->name, s_name) < 0)
			first = middle + 1;
		else if(strcmp(name_array[middle]->name, s_name) == 0)	
			return middle;
		
		/* Name is in top half of array bounds */
		else
			last = middle - 1;

		/* Recalculate middle value with new bounds */
		middle = ((last-first)/2) + first;
	}
	if(first > last)
		return -1;
}




