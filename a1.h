#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#ifndef A1_H
#define A1_H

/* Constant Defs */
#define MAX_LINE 500
#define HASH_VALUE 9013

/* typedefs */
typedef enum truefalse
{
	FALSE, TRUE
} BOOLEAN;
typedef struct name_list * name_list_ptr;
typedef struct name_data ** name_data_ptr;
typedef struct hash_table * hash_table_ptr;

/* Function used for time-based benchmarking.
 * Taken From Week 3 Lab Sheet. */
unsigned long gettime();

/* Other Header Files */
#include "a1_io.h"
#include "a1_linked.h"
#include "a1_array.h"
#include "a1_hash.h"
#endif
