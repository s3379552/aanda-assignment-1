#ifndef A1_LINKED_H
#define A1_LINKED_H
#include "a1.h"

/* Linked List typedefs */
typedef struct name_node {
	char * name;
	char * origin;
	char * meaning;
	struct name_node * next_node;
} name_node;

typedef struct name_list {
	int count;
	struct name_node * head;
} name_list;

/* Function for creating the linked list. Function allocates memory
 * for one element in the linked list and creates the head pointing
 * to null. The new list is returned. */
struct name_list create_list();

/* Function for adding a new node to the linked list. The funtion takes in a
 * already initialised node, and iterates through the linked list. In this
 * version of the function, the iterations stop once the new node's name
 * is lesser than the current node's name. If the position reached is the
 * start of the list, then the new node is assigned as the head of the list.
 * Otherwise, the new node takes the current node's position in the list, 
 * moving the current node (and all trailing nodes) down one position in 
 * the list. To use this function without sorting, the new node would
 * take the head node's position. */
struct name_list * add_node(name_node * new_node,  name_list *  n_list);

/* Fuction for sorting the linked list (if not sorted in add_node function).
 * The function implements a bubble sort. It iterates through the linked list,
 * comparing adjacent nodes. If the second node's name is less than the first
 * node's name, they are swapped. The whole list is iterated through in this 
 * manner. These passes continue until no swaps are made.If no swaps take 
 * place in an entire pass of the list, then the list is in order and the 
 * function is ended.*/
struct name_list * sort_list(name_list * n_list);

/* Functions for searching through the list to find the passed name.
 * The "dumb" function will iterate through the entire list unti either the
 * name is found (the corresponding node is returned), or the end of the list
 * is reached (NULL is returned). The "smart" search assumes the list is sorted
 * and compares the passed name to the current name in the list. If the passed
 * name is lesser than the current name, it cannot be later in the list and 
 * NULL is reutrned. If the name is found then the cooresponding node is 
 * returned.*/
struct name_node * smart_search_node(name_list * n_list, char * search_name);
struct name_node * dumb_search_node(name_list * n_list, char * search_name);
#endif
