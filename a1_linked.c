#include "a1_linked.h"

struct name_list create_list()
{
	name_list  new_list;
	
	/* Assigning head of list to null*/
	new_list.head = NULL;
	new_list.count = 1;
	return new_list;
}

struct name_list * add_node(name_node * new_node, name_list * n_list)
{
	struct name_node * current_node = n_list->head;
	struct name_node * previous_node = NULL;
	int head_flag = TRUE;

	/* Reallocate space in list for current size + 1 more node */
	n_list = realloc(n_list, sizeof(n_list) + 
		((n_list->count+1)*sizeof(struct name_node)));
	assert(n_list != NULL);

	/* Iterating through list until end is reached.*/
	while(current_node != NULL)
	{
		/* CODE OF SORTING WITH INSERTION */
		if(strcmp(new_node->name, current_node->name) < 0)
		{
			break;
		}
		previous_node = current_node;
		current_node = current_node->next_node;

		/* If at least one iteration is made, the head of the list does
 * 		not need to be changed.*/
		head_flag = FALSE;
	}

	/* Changing value of the head node */
	if(head_flag == TRUE)
	{
		new_node->next_node = n_list->head;
		n_list->head = new_node;
		return n_list;
	}

	/* Inserting node is postion other than head */
	else
	{
		new_node->next_node = current_node;
		previous_node->next_node = new_node;
		n_list->count++;
		return n_list;
	}
}

/* Code structure taken from
 * faculty.cs.niu.edu/~mcmahon/CS241/Notes/search.html*/
struct name_node * smart_search_node(name_list * n_list, char *search_name)
{
	struct name_node * current = NULL;
	double count = 0;

	/* Iterate through list until passed name is less than current name */
	for(current = n_list->head; current != NULL && 
		strcmp(search_name, current->name) < 0; 
		current = current->next_node)
		;

	/* If the passed name and current name are not equal, the passed name
 * 	is not in the list */
	if(current != NULL && strcmp(search_name, current->name) == 0)
	{	
		return current;
	}
	else
	{
		return NULL;
	}
}

struct name_node * dumb_search_node(name_list * n_list, char * search_name)
{
	struct name_node * current;
	int print_choice;
	/* Iterate through list until matching name is found or end of list
 * 	is reached */
	for(current = n_list->head; current != NULL; current = current->next_node)
	{
		if(strcmp(search_name, current->name) == 0)
		{
			return current;
		}
	}
	return NULL;
}

struct name_list * sort_list(struct name_list * n_list)
{
	/* BUBBLE SORT */
	struct name_node * previous_node = NULL;
	struct name_node * current_node = n_list->head;
	struct name_node * forward_node = n_list->head->next_node;
	int sorted_list = FALSE;

	/* Iterate passes through whole list until no positions are changed*/
	while(sorted_list == FALSE)
	{
		sorted_list = TRUE;
		previous_node = NULL;
		current_node = n_list->head;
		forward_node = n_list->head->next_node;

		/* Single pass through list */
		while(forward_node != NULL)
		{
			/* Check to see if two adjacent nodes are out of
 * 			position */
			if(strcmp(current_node->name, forward_node->name) > 0)
			{
				/* Indicate list is not sorted and another pass
 * 				is to be made */
				sorted_list = FALSE;
				
				/* Swapping nodes */
				/* If current position is head of list */
				if(previous_node == NULL)
				{
					current_node->next_node = forward_node->next_node;
					forward_node->next_node = current_node;
				}
		
				/* All other positions */
				else
				{
					previous_node->next_node = forward_node;
					current_node->next_node = forward_node->next_node;
					forward_node->next_node = current_node;
				}
				/*iterate to next pair */
				previous_node = forward_node;
				forward_node = current_node->next_node;
			}

			/*Move to next two nodes in list */
			else
			{
				previous_node = current_node;
				current_node = forward_node;
				forward_node = forward_node->next_node;
			}		
		}
	}
	return n_list;
}
