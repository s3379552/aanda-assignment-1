#include "a1.h"

unsigned long gettime()
{
	struct timeval tp;
	gettimeofday(&tp,NULL);
	return(tp.tv_sec*1E6) + tp.tv_usec;
}

int main(void)
{
	int a_size = 0, * a_size_ptr; 
	int i, max_value = 0, max_buckets = 0, zero_count = 0, ave_buckets, total_buckets = 0;
	double hash_size = 0;
	unsigned long start_time, end_time, total_time;
	name_data_ptr name_array = create_array();
	name_list_ptr nlptr = NULL;
	name_node * current_node, * prev_node;
	hash_table_ptr hptr = NULL;
	hash_bucket * current_bucket, * prev_bucket;
	hash_entry * current_entry, * prev_entry;
	char * namesfile = "names.txt";
	char * searchfile = "search.txt";
	nlptr = malloc(sizeof(name_list));
	hptr = malloc(sizeof(hash_table));
	a_size_ptr = &a_size;
	*nlptr = create_list();
	*hptr = create_table();
	printf("\n--LINKED LIST INSERTION--\n\n");
	nlptr = read_names_list(namesfile, nlptr);
	printf("\nSize of linked list: %.2f Kb\n", (double) (sizeof(nlptr) + 
		(nlptr->count*sizeof(struct name_node)))/1000);
	printf("\n-------------------------\n");
	printf("\n--ARRAY INSERTION--\n\n");
	name_array = read_names_array(namesfile, name_array, a_size_ptr);
	printf("\nSize of array: %.2f Kb\n", (double) (sizeof(struct name_data)
		 * *a_size_ptr)/1000);
	printf("\n-------------------\n");
	printf("\n--HASH TABLE HASHING/INSERTION--\n\n");
	hptr = read_names_hash(namesfile, hptr);
	hash_size += (double) (sizeof(hptr) + (sizeof(hash_value) * HASH_VALUE));
	for(i = 0; i < HASH_VALUE; i++)
	{
		hash_size += (double) hptr->table[i]->buckets * sizeof(struct hash_bucket);
		current_bucket = hptr->table[i]->first_bucket;
		while(current_bucket != NULL)
		{
			hash_size += (double) current_bucket->entries * 
				sizeof(struct hash_entry);
			current_bucket = current_bucket->next_bucket;
			
		}
	}
	printf("Size of hash table: %.2f Kb\n", (double) hash_size/1000);
	printf("\n--------------------------------\n");
	printf("\n--HASH TABLE DISTRIBUTION--\n\n");
	for(i = 0; i < HASH_VALUE; i++)
	{
		if(hptr->table[i]->buckets == 0)
			zero_count++;
		if(hptr->table[i]->buckets > max_value)
		{
			max_value = hptr->table[i]->buckets;
			max_buckets = 0;
		}
		if(hptr->table[i]->buckets == max_value)
			max_buckets++;
		total_buckets += hptr->table[i]->buckets; 
		
	} 
	ave_buckets = total_buckets/(HASH_VALUE-zero_count);
	printf("Max Bucket Size: %d\n", max_value);
	printf("Max Buckets Occurances: %d\n", max_buckets);
	printf("Average Bucket Size: %d\n", ave_buckets);
	printf("Zero Bucket Occurances: %d\n", zero_count); 
	printf("\n---------------------------\n");	
	printf("\n--LINKED LIST SORTING--\n");
	printf("\nSORTING DONE WITHIN INSERTION.\n.");
	/*start_time = gettime();
	nlptr = sort_list(nlptr);
	end_time = gettime();
	total_time = end_time - start_time;
	printf("\nLIST SORT TIME: %.2f\n", (double)total_time/1E6);*/
	printf("\n-----------------------\n");
	printf("\n--ARRAY SORTING--\n");
	printf("\nQUICKSORT FUNCTION USED.\n");
	start_time = gettime();
	name_array = qsort_array(name_array, 0, *a_size_ptr);
	end_time = gettime();
	total_time = end_time - start_time;
	printf("\nARRAY QUICKSORT TIME: %.4f\n", (double)total_time/1E6);
	printf("\n-----------------\n");	
	printf("\n--LINKED LIST SEARCHING--\n\n");
	search_list(searchfile, nlptr);
	printf("\n-------------------------\n");
	printf("\n--ARRAY SEARCHING--\n\n");
	search_array(searchfile, name_array, a_size_ptr);
	printf("\n-------------------\n");
	printf("\n--HASH TABLE SEARCHING--\n\n");
	search_table(searchfile, hptr);
	printf("\n------------------------\n");
	
	/* Free memory */
	/* Linked List */
	current_node = nlptr->head;
	prev_node = NULL;
	while(current_node != NULL)
	{
		free(current_node->name);
		free(current_node->origin);
		free(current_node->meaning);
		prev_node = current_node;
		current_node = current_node->next_node;
		free(prev_node);
	}
	free(nlptr);
	
	/* Array */
	for(i = 0; i < *a_size_ptr; i++)
	{
		free(name_array[i]->name);
		free(name_array[i]->origin);
		free(name_array[i]->meaning);
	}
	free(name_array);
	
	/* Hash Table*/
	prev_bucket = NULL;
	prev_entry = NULL;
	for(i = 0; i < HASH_VALUE; i++)
	{
		current_bucket = hptr->table[i]->first_bucket;
		if(current_bucket != NULL)
			current_entry = current_bucket->first_entry;
		while(current_bucket != NULL)
		{
			while(current_entry != NULL)
			{
				free(current_entry->name);
				free(current_entry->origin);
				free(current_entry->meaning);
				prev_entry = current_entry;
				current_entry = current_entry->next_entry;
				free(prev_entry);
			}
			free(current_bucket->name);
			prev_bucket = current_bucket;
			current_bucket = current_bucket->next_bucket;
			free(current_bucket);
		}
		free(hptr->table[i]);
	}
	free(hptr);
	return EXIT_SUCCESS;





}
