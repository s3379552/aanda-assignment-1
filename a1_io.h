#ifndef A1_IO_H
#define A1_IO_H
#include "a1.h"

/* Fuctions for adding data from text files to their respective data structures
 * Each of these functions operate in the same way for extracting data from
 * the text files, the adds the data to a newly created instance of the 
 * appropriate element container. The text file is first opened and a line
 * of text is retrieved. The line is then tokenised into seperate strings
 * for the name, origin and meaning. These strings are copied into the new
 * element container. The insertion method is then timed and called, with the
 * element's insertion time added to the total insertion time. The total 
 * insertion time is then displayed.*/
struct name_list * read_names_list(char *filename, name_list_ptr n_list);
struct name_data ** read_names_array(char *filename, name_data_ptr n_array, int *size);
struct hash_table * read_names_hash(char * filename, hash_table_ptr n_table);

/* Functions for searching for specified names (taken from a text file).
 * The extraction of the names from the file operates similarly to the
 * same file access code from the read_names functions. The names are read
 * as one line per name, this line is tokenised (to get rid of extra 
 * characters after the name) and the token is passed to the search 
 * function for the data structure. The search functions (dumb and smart
 * searches) are timed, with the total time for all search functions displayed 
 * at the end of the function. */
void  search_list(char * filename, name_list_ptr  n_list);
void  search_array(char * filename, name_data_ptr n_array, int * size);
void  search_table (char * filename, hash_table_ptr n_table);
#endif
