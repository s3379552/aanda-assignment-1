#ifndef A1_ARRAY_H
#define A1_ARRAY_H
#include "a1.h"

/* Array element typedef */
typedef struct name_data{
	char * name;
	char * origin;
	char * meaning;
}name_data;

/* Function for initialising array. Memory is allocated for one element.*/
struct name_data ** create_array();

/* Function for adding name to end of array. Memory is reallocated for array as
 * existing array size plus space for one more element. The new element is 
 * inserted at end of array (known due to passed size tracker), and size tracker
 * is incremented by one.*/
struct name_data ** add_name(name_data * new_name, name_data ** name_array, int * size);

/* Function for sorting array using a quicksort. The middle of the current 
 * array bounds is found, and the name of the element at this middle position
 * is taken as the pivot. While the current left side tracker is less than the
 * right side tracker, two loops sequentially happen. The current left positon
 * name is compared to the pivot, moving the left position up the array until
 * a name greater than the pivot is found. The same is done for the right side
 * tracker, while the current name is greater than the pivot it moves backwards
 * through the array. Once both of these loops have stopped, if the left
 * tracker position is less or equal to the right tracker position, the 
 * elements at these positions are swapped. Once the left tracker is greater
 * than the right tracker (both side of pivot have been passed across once), 
 * the function is called recursively if the passed left value is less than
 * the right tracker (left half of passed array bound is then sorted with same
 * function), then if the right side value is lesser than the left tracker
 * (right side of passed array bound is then sorted with same function).
 * Once all recursive calls of the function have been completed, the whole
 * array is sorted.*/ 
struct name_data ** qsort_array(name_data ** name_array, int left, int right);

/*Function for sorting array with an insertion sort. Iterating through the 
 * whole array, each value of the array is taken as the comparison value. Every
 * one of these comparison values is compared against the whole rest of the
 * array after it while the compared value is less than the current array value.
 * While this is true, each position in the array is swapped with its changed to
 * its proceeding value. After this loop, the current position is then made to
 * equal the comparison value, placing this vaule in its correct array position.
 * Once this is done for all array values, the array is sorted*/
struct name_data ** isort_array(name_data ** name_array, int * max);

/* Function for searching through the array with a "dumb" sort. The function
 * iterates through the array, comparing each name against the pased name. If a
 * match is found, the current element is returned. If the end of the array is
 * reached, the element does not exist in the array and -1 is returned.*/
int dumb_search_array(name_data ** name_array, char * s_name, int * max);

/* Function for searching through the array with a binary search. The function
 * calculates the middle of the array, and keeps track of the left and right
 * positions, which start at the start and end of the array respectively. 
 * While the right value is greater than the left value, if the middle element
 * is less than the requested name, the left value is changed to the middle 
 * value (Halving the search area). If they are equal, the position is found and
 * returned. Otherwise, the last value is changed to the middle value (halving
 * the search area as well, to the bottom half). If the end of the loop 
 * iteration is reached, the middle is recalculated with the new left/right 
 * value. If the loop is broken (array cant be halved anymore), then the name
 * has not been found and does not exist in the array, so -1 is returned.*/
int smart_search_array(name_data ** name_array, char * s_name, int * max);
#endif
