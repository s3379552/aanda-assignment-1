#ifndef A1_HASH_H
#define A1_HASH_H
#include "a1.h"

/* Hash Table typedefs */
typedef struct hash_entry {
	char * name;
	char * origin;
	char * meaning;
	struct hash_entry * next_entry;
} hash_entry;

typedef struct hash_bucket {
	int entries;
	char * name;
	hash_entry * first_entry;
	struct hash_bucket * next_bucket;
} hash_bucket;

typedef struct hash_value {
	int buckets;
	hash_bucket * first_bucket;
} hash_value;
typedef struct hash_table {
	int size;
	hash_value ** table;
} hash_table;


/* Function to initialise hash table. Creates a hash table with a number of
 * elements defined by a hash constant. For each array element, memory is
 * allocated for a bucket linked list inside it. */
struct hash_table create_table();

/* Function for inserting a new name in the hash table (post-hashing). First
 * the array position of the new name's hash value is checked for NULL. If
 * the linked list is null, a new bucket is created for the new name in that
 * hash position as the first bucket. Otherwise, the bucket linked list is
 * iterated through. If the same name is found, the new entry is added into
 * the existing bucket. If the new name is less than the current bucket's name,
 * the a new bucket is created for the name and it is inserted at the current
 * bucket's position, which is moved down the linked list. If the new name is
 * greater than all existing bucket names, it is inserted at the end of the
 * bucket linked list in a new bucket. */
struct hash_table * hash_insert(hash_entry * new_entry, hash_table * h_table, 
	int h_value);

/* Function for looking up the requested name, assuming ordered buckets. First,
 * the new name's hash value is calculated. The bucket linked list at that 
 * hash value is then iterated, through. If the current bucket's name is 
 * lesser than the requested name, the name doesn't exist in the hash table
 * and NULL is returned. If the name is found, the name bucket is returned.
 * If the end of the linked list is reached, the name doesn't exist and NULL
 * is returned. */
struct hash_bucket * hash_smart_lookup(hash_table * h_table, char * name);

/* Function for looking up the requested name, with no assumptions. This
 * function operates in a similar manner to the smart function, without
 * the comparision to see if the current position name is lesser than
 * the requested name, so missing names will always reach the end of the
 * bucket linked list before NULL is returned. */
struct hash_bucket * hash_dumb_lookup(hash_table * h_table, char * name);

/* Function for generating the hash value of a given name. This currently
 * uses Kernighan and Ritchie's function. This takes an inital value, 
 * and a constant multiplier. For every character in the given name, the
 * current value (starting at the initial value) is mulitplied by the
 * constant multiplier, then the ASCII value of the current character is
 * added to this number. The end result is then modular divided by the
 * constant hash value. The other hash function present is a simple
 * addition function, adding the ASCII values of all characters in the word
 * together, then modular dividing this total by the constant hash value. */
int hash_gen(char * name);
#endif
