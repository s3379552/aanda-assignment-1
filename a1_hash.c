#include "a1_hash.h"

struct hash_table create_table()
{
	int i;
	hash_table h_table;
	hash_value * h_value;
	h_table.size = HASH_VALUE;
	printf("---------------------------");
	printf("\n--HASH SIZE %d SELECTED--\n", h_table.size);
	printf("---------------------------\n");

	/* Hash table memory allocated for table itself and all bucket linked
 * 	lists.*/
	h_table.table = malloc(sizeof(struct hash_value)*HASH_VALUE);
	for(i = 0; i < h_table.size; i++)
	{
		h_value = malloc(sizeof(struct hash_value));
		h_table.table[i] = h_value;
		h_table.table[i]->buckets = 0;
		h_table.table[i]->first_bucket = NULL;
	}
	return h_table;
}

struct hash_table * hash_insert(hash_entry * new_entry, hash_table * h_table, int h_value)
{
	hash_bucket * new_bucket;
	hash_bucket * current_bucket, * prev_bucket;
	hash_entry * current_entry;
	
	/* No Buckets in current hash value */
	if(h_table->table[h_value]->first_bucket == NULL)
	{
		new_bucket = malloc(sizeof(hash_bucket));
		new_bucket->first_entry = new_entry;
		new_bucket->entries = 1;
		new_bucket->next_bucket = NULL;
		new_bucket->name = malloc(strlen(new_entry->name)*sizeof(char));
		strcpy(new_bucket->name, new_entry->name);
		h_table->table[h_value]->buckets = 1;
		h_table->table[h_value]->first_bucket = new_bucket;
	}
	else
	{
		prev_bucket = NULL;
		current_bucket = h_table->table[h_value]->first_bucket;
		while(current_bucket != NULL)
		{
			/* New name smaller than current bucket name, bucket
 * 			for new name doesn't exist */
			if(strcmp(new_entry->name, current_bucket->name) < 0)
			{
				new_bucket = malloc(sizeof(hash_bucket));
				new_bucket->first_entry = new_entry;
				new_bucket->entries = 1;
				new_bucket->next_bucket = current_bucket;
				new_bucket->name = malloc(strlen(new_entry->name)*sizeof(char));
				strcpy(new_bucket->name, new_entry->name);
				h_table->table[h_value]->buckets += 1;
				if(prev_bucket == NULL)
				{
					h_table->table[h_value]->first_bucket = new_bucket;
				}
				else
				{
					prev_bucket->next_bucket = new_bucket;
				}
				return h_table;

			}
			/* Bucket for new name already exists*/
			else if(strcmp(current_bucket->name, new_entry->name) == 0)
			{
				current_entry = current_bucket->first_entry;
				while(current_entry != NULL)
				{
					current_entry = current_entry->next_entry;
				}
				current_entry = new_entry;
				current_bucket->entries += 1;
				return h_table;
			}
			prev_bucket = current_bucket;
			current_bucket = current_bucket->next_bucket;
		}

		/* Bucket for new name doesn't exist, and is greater than
 * 		all current names in bucket list*/
		new_bucket = malloc(sizeof(hash_bucket));
		new_bucket->first_entry = new_entry;
		new_bucket->entries = 1;
		new_bucket->next_bucket = NULL;
		new_bucket->name = malloc(strlen(new_entry->name)*sizeof(char));
		strcpy(new_bucket->name, new_entry->name);
		h_table->table[h_value]->buckets += 1;
		prev_bucket->next_bucket = new_bucket;
		return h_table;
	}
	return h_table;
}

struct hash_bucket * hash_smart_lookup(hash_table * h_table, char * name)
{

	/* Hash for request name generated */
	int hash = hash_gen(name);
	hash_bucket * current_bucket = h_table->table[hash]->first_bucket;
	if(current_bucket == NULL)
		return NULL;
	while(current_bucket != NULL)
	{
		/* If new name is greater than current name, doesn't exist
 * 		in list */
		if(strcmp(name, current_bucket->name) > 0)
			return NULL;
		if(strcmp(name, current_bucket->name) == 0)
			return current_bucket;
		current_bucket = current_bucket->next_bucket;
	}
	return NULL;
	
}

struct hash_bucket * hash_dumb_lookup(hash_table * h_table, char * name)
{
	int hash = hash_gen(name);
	hash_bucket * current_bucket = h_table->table[hash]->first_bucket;
	if(current_bucket == NULL)
		return NULL;

	/* Iterates through list unitl matching name is found or end of
 * 	list is reached. */
	while(current_bucket != NULL)
	{
		if(strcmp(name, current_bucket->name) == 0)
			return current_bucket;
		current_bucket = current_bucket->next_bucket;
	}
	return NULL;
	
}

int  hash_gen(char * name)
{
	/* Addition Hash */
	/*double total_hash = 1;*/
	/*int total_hash = 0;
	int i, final_hash;
	for(i = 0; i < strlen(name); i++)
	{
		total_hash += name[i];
	}
	final_hash = total_hash % HASH_VALUE;
	return final_hash;*/

	/* Kernighan and Ritchie's function exaple from
 * 	www.strchr.com/hash_functions */
	int init_value = 0;
	long int multiplier = 31;
	int i; 
	unsigned int final_hash;
	unsigned long int total_multi;
	total_multi = init_value;
	for(i = 0; i < strlen(name); i++)
	{
		total_multi = multiplier * total_multi + (long int) name[i];
	}
	final_hash = total_multi % HASH_VALUE;
	return final_hash;
}
